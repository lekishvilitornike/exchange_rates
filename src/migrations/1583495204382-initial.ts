import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1583495204382 implements MigrationInterface {
    name = 'initial1583495204382';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `fiat_currencies` CHANGE `rate` `rate` double NOT NULL DEFAULT 0", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `fiat_currencies` CHANGE `rate` `rate` double NOT NULL", undefined);
    }

}
