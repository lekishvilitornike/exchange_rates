import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1583490056399 implements MigrationInterface {
    name = 'initial1583490056399';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `fiat_currencies` ADD `rate` double NOT NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `fiat_currencies` DROP COLUMN `rate`", undefined);
    }

}
