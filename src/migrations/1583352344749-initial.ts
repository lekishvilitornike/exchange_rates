import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1583352344749 implements MigrationInterface {
    name = 'initial1583352344749';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `crypto_currencies` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `fiat_currencies` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `exchange_rates` (`id` int NOT NULL AUTO_INCREMENT, `rate` double NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `cryptoCurrencyId` int NULL, `fiatCurrencyId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `exchange_rates` ADD CONSTRAINT `FK_47c1535d1e1c7db1705cc7bc659` FOREIGN KEY (`cryptoCurrencyId`) REFERENCES `crypto_currencies`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `exchange_rates` ADD CONSTRAINT `FK_f0a8f4f1c05f8ffb4922c98da40` FOREIGN KEY (`fiatCurrencyId`) REFERENCES `fiat_currencies`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `exchange_rates` DROP FOREIGN KEY `FK_f0a8f4f1c05f8ffb4922c98da40`", undefined);
        await queryRunner.query("ALTER TABLE `exchange_rates` DROP FOREIGN KEY `FK_47c1535d1e1c7db1705cc7bc659`", undefined);
        await queryRunner.query("DROP TABLE `exchange_rates`", undefined);
        await queryRunner.query("DROP TABLE `fiat_currencies`", undefined);
        await queryRunner.query("DROP TABLE `crypto_currencies`", undefined);
    }

}
