import express, {NextFunction, Request, Response} from "express";
import {createConnection, getRepository} from "typeorm";
import bodyParser from "body-parser";
import cors from 'cors';
import {connectionOptions} from "./config/connections.option";
import {CryptoCurrencyService, ExchangeRateService, FiatCurrencyService} from "./services";
import cron from "node-cron";
import {BitGoParserService} from "./services/bitGoParser.service";


const port = Number(process.env.APP_PORT || 3000);
(async () => {
    const connection = await createConnection(connectionOptions);
    const app = express();
    app.use(cors());
    app.use(bodyParser.json());


    const cryptoCurrencyService = new CryptoCurrencyService(connection);
    const fiatCurrencyService = new FiatCurrencyService(connection);
    const exchangeRateService = new ExchangeRateService(connection);
    const bitGoParserService = new BitGoParserService(cryptoCurrencyService, fiatCurrencyService);

    cron.schedule('*/5 * * * *', async () => {
        const results = await bitGoParserService.getRates();
        await exchangeRateService.create(results);
    });
    cron.schedule('*/10 * * * *', async () => {
        await fiatCurrencyService.getExchangeRateFromNgb();
    });
    cron.schedule('*/15 * * * *', async () => {
        await fiatCurrencyService.getExchangeRates();
    });

    app.get('/exchangeRates', async (req: Request, res: Response) => {
        const [fiatCurrency, cryptoCurrencies] = await Promise.all([
            fiatCurrencyService.getByName(req.query.fiatCurrencyName),
            req.query.cryptoCurrencyNames
                ? cryptoCurrencyService.getByNames(req.query.cryptoCurrencyNames)
                : cryptoCurrencyService.getAll()
        ]);
        const rates = await exchangeRateService.getExchangeRates(fiatCurrency, cryptoCurrencies);
        res.json(rates);
    });

    app.get('/test', async (req: Request, res: Response) => {
        await fiatCurrencyService.getExchangeRateFromNgb();
    });


    app.get('/changeInfo', async (req: Request, res: Response) => {
        const fiatCurrency = await fiatCurrencyService.getByName(req.query.fiatCurrencyName);
        const cryptoCurrency = await cryptoCurrencyService.getByName(req.query.cryptoCurrencyName);
        const dateFrom = new Date(req.query.dataFrom);
        const dateTo = new Date(req.query.dateTo);

        const results = await exchangeRateService.getDiff(fiatCurrency, cryptoCurrency, dateFrom, dateTo);

        res.json(results);
    });

    app.listen(port, () => {
        console.log(`Server started on http://localhost:${port}`);
    });
})();
