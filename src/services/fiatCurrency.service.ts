import {Connection, In, Repository} from "typeorm";
import {FiatCurrencyEntity} from "../entities";
import * as soap from 'soap';
import {Client} from "soap";
import {exchangeRate, nbgEndpoint} from "../config/endpoint";
import axios from 'axios';


interface ExchangeRates {
    rates: Record<string, number>
    base: string,
    date: string,
}

interface ExchangeRate {
    return: {
        attributes: {},
        $value: string
    },
}

export class FiatCurrencyService {

    private repo: Repository<FiatCurrencyEntity>;

    constructor(private connection: Connection) {
        this.repo = connection.getRepository(FiatCurrencyEntity);
    }

    async getAll() {
        return this.repo.find({});
    }

    async getByName(name: string) {
        return await this.repo.findOne({name: name});
    };

    async getExchangeRates() {
        const rates = await axios.get<ExchangeRates>(exchangeRate()).then(x => x.data);
        await this.getAll().then(entities => {
            return entities.map(entity => {
                const rate = rates.rates[entity.name];
                if (rate) {
                    entity.rate = rate;
                    this.repo.save(entity);
                }
            })
        });
    }

    async getExchangeRateFromNgb(currency: string = "USD") {
        const client: Client = await soap.createClientAsync(nbgEndpoint());
        const gelEntity = await this.repo.findOne({name: "GEL"});
        if (!gelEntity) return;
        await client.GetCurrency({currency}, (error: any, res: ExchangeRate) => {
            gelEntity.rate = Number(res.return.$value);
            this.repo.save(gelEntity);
        });
    }
}
