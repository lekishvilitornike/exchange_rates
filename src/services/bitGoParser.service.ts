import {CryptoCurrencyService, ExchangeRateService, FiatCurrencyService} from "./index";
import axios from "axios";
import {bitGoEndpoint} from "../config/endpoint";
import {CryptoCurrencyEntity, FiatCurrencyEntity} from "../entities";


interface BitGoDto {
    latest: {
        currencies: Record<string, { last: number }>
    }
}

export class BitGoParserService {

    constructor(private cryptoCurrencyService: CryptoCurrencyService,
                private fiatCurrencyService: FiatCurrencyService) {

    }

    async exchangeRate(rateName: string, usdRate: number) {
        const rate = await this.fiatCurrencyService.getByName(rateName).then(x => x.rate);
        return rate ? rate * usdRate : 0;
    }

    public async getRates() {
        const cryptoCurrencies: CryptoCurrencyEntity[] = await this.cryptoCurrencyService.getAll();
        const fiatCurrencies: FiatCurrencyEntity[] = await this.fiatCurrencyService.getAll();

        let promises = cryptoCurrencies.map(async cryptoCurrency => {
            const res = await axios.get<BitGoDto>(bitGoEndpoint(cryptoCurrency.name));
            let promise = fiatCurrencies.map(async fiatCurrency => {

                const checkRate = res.data.latest.currencies[fiatCurrency.name] ?
                    res.data.latest.currencies[fiatCurrency.name].last :
                    await this.exchangeRate(fiatCurrency.name, res.data.latest.currencies.USD.last);

                return {
                    rate: checkRate,
                    fiatCurrency,
                    cryptoCurrency
                }
            });
            return Promise.all(promise);
        });
        const result = await Promise.all(promises);
        return [].concat(...result);
    }
}


