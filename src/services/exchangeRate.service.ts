import {Connection, LessThan, Repository} from "typeorm";
import {CryptoCurrencyEntity, ExchangeRateEntity, FiatCurrencyEntity} from "../entities";

export class ExchangeRateService {

    private repo: Repository<ExchangeRateEntity>;

    constructor(private connection: Connection) {
        this.repo = connection.getRepository(ExchangeRateEntity);
    }

    async getExchangeRates(fiatCurrency: FiatCurrencyEntity, cryptoCurrencies: CryptoCurrencyEntity[]) {
        const promises = cryptoCurrencies.map(cryptoCurrency => {
            return this.repo.findOne({
                where: {
                    fiatCurrency,
                    cryptoCurrency
                },
                order: {
                    createdAt: "DESC"
                }
            });
        });
        return Promise.all(promises);
    }

    async getCurrentRate(fiatCurrency: FiatCurrencyEntity,
                         cryptoCurrency: CryptoCurrencyEntity,
                         date: Date) {
        return await this.repo.findOne({
            where: {
                createdAt: LessThan(date),
                cryptoCurrency,
                fiatCurrency,
            },
            order: {
                createdAt: "DESC"
            },
        }).then(x => x.rate);
    }

    async getDiff(fiatCurrency: FiatCurrencyEntity,
                  cryptoCurrency: CryptoCurrencyEntity,
                  dateFrom: Date,
                  dateTo: Date) {
        const from = await this.getCurrentRate(fiatCurrency, cryptoCurrency, dateFrom);
        const to = await this.getCurrentRate(fiatCurrency, cryptoCurrency, dateTo);

        return ((to - from) / from) * 100;
    }

    async create(data: ExchangeRateEntity[]) {
        return await this.repo.save(data);
    }
}
