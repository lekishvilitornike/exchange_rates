import {Connection, getRepository, In, Repository} from "typeorm";
import {CryptoCurrencyEntity} from "../entities";


export class CryptoCurrencyService {

    private repo: Repository<CryptoCurrencyEntity>;

    constructor(private connection: Connection) {
        this.repo = connection.getRepository(CryptoCurrencyEntity);
    }

    async getAll() {
        return this.repo.find({});
    }

    async getByName(name: string) {
        return await this.repo.findOne({name: name});
    };

    async getByNames(names: string[]) {
        return await this.repo.find({
            name: In(names)
        });
    };
}

