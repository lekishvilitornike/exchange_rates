import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";


@Entity('fiat_currencies')
export class FiatCurrencyEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column({type: "double", default: 0})
    public rate: number;
}
