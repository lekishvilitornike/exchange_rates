import {Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {CryptoCurrencyEntity} from "./cryptoCurrency.entity";
import {FiatCurrencyEntity} from "./fiatCurrency.entity";

@Entity('exchange_rates')
export class ExchangeRateEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne(type => CryptoCurrencyEntity)
    public cryptoCurrency: CryptoCurrencyEntity;

    @ManyToOne(type => FiatCurrencyEntity)
    public fiatCurrency: FiatCurrencyEntity;

    @Column({type: "double"})
    public rate: number;

    @CreateDateColumn()
    public createdAt: Date;
}
