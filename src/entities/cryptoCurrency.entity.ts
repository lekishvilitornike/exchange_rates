import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {ExchangeRateEntity} from "./exchangeRate.entity";


@Entity('crypto_currencies')
export class CryptoCurrencyEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;
}
