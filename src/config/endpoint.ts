export const bitGoEndpoint = (cryptoCurrency: string) => {
    return `https://www.bitgo.com/api/v2/${cryptoCurrency}/market/latest`;
};


export const exchangeRate = (fiatCurrency: string = 'USD') => {
    return `https://api.exchangeratesapi.io/latest?base=${fiatCurrency}`
};

export const nbgEndpoint = () => {
    return 'http://nbg.gov.ge/currency.wsdl';
};
