import {ConnectionOptions} from "typeorm";
import {CryptoCurrencyEntity, ExchangeRateEntity, FiatCurrencyEntity} from "../entities";
import {initial1583352344749} from "../migrations/1583352344749-initial";
import {initial1583490056399} from "../migrations/1583490056399-initial";
import {initial1583495204382} from "../migrations/1583495204382-initial";

export const connectionOptions: ConnectionOptions = {
    type: "mysql",
    host: String(process.env.DB_HOST || 'localhost'),
    port: Number(process.env.DB_POST || 3306),
    username: String(process.env.DB_USERNAME || 'root'),
    password: String(process.env.DB_PASSWORD || 'root'),
    database: String(process.env.DB_DATABASE || 'exchange_rate'),
    migrationsRun: true,
    entities: [
        ExchangeRateEntity,
        CryptoCurrencyEntity,
        FiatCurrencyEntity
    ],
    migrations: [
        initial1583352344749,
        initial1583490056399,
        initial1583495204382
    ],
    cli: {
        migrationsDir: "./src/migrations"
    },
    logging: false
};
